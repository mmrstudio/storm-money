<div class="about">
	<div class="left">
		<div class="lcontent">
		<?php echo get_field('left','option');?>
		<div class="blue-button"><button class="blue-btn"> Speak to us now</button> </div>

		<div class="localbroker"> <a href=""><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/footer.svg"> </a></div>
	 </div>
	</div>

	<div class="right">

	<?php $right =  get_field('right','option');
		foreach($right as $content) { ?>

			<div class="col-xs-12 rcontent">
				<div class="about_title"><?php echo $content['title'];?> </div>
				<div class="text"><?php echo $content['text'];?> </div>
			</div>

	<?php }
	?>
	 </div>

</div>