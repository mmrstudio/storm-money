<style type="text/css">
 @media (min-width: 768px) {
	header {
		background:url('<?php echo get_field('banner_image','option');?>') top center;
		background-size: cover;
	}
}
</style>
<header>
  <div class="overlay"></div>
 
 <div class="title" id="main-logo">
 		<div class="banner-title col-xs-12">Home Loan Rate of the Day </div>
 		<div class="banner-text ">

 			<div class=""><?php echo get_field('interest_rate','option'); ?> 	</div>
 			<div class="bolt-banner"><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/banner-bolt.png"> </div>
 			<div class=""><?php echo get_field('interest_rate_right','option'); ?></div>

 		</div>
 		<div class="terms">
 			<ul>	
 				<?php $terms = get_field('terms','option');
 				foreach ($terms as $term) {
 					?>
 					<li><span><?php echo $term['text'];?></span></li>

 					<?php

 				}?>
 				<!-- <li><span>Up to 80% Loan to Value</span></li>
 				<li><span>No Ongoing Fee's</span></li>
 				<li><span>Free Redraw</span></li> -->
 			</ul>
 		</div>
 		<button class="blue-btn"> Speak to us now</button>
  		<div class="bottom-text"> Applications for credit are subject to credit approval criteria. Terms and conditions apply. Rates are subject to change. </div>
  </div>
</header>

 