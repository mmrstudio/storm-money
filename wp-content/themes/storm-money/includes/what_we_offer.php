<div class=" what_we_offer">

<div class="container">
  <div class="col-xs-12 ">
     <h1><?php echo get_field('title_about','option'); ?> </h1>
    		 <?php echo get_field('about_text','option');?>
</div>
</div>

     <div class="services">
<?php $services = get_field('services','option');
	
	foreach($services as $service) { ?>

		<div class="col-lg-3 col-md-6  col-sm-6 col-xs-12 background" style="background: linear-gradient(to bottom, rgba(89, 43, 140, 0.53), rgba(3, 21, 65, 0.93)), 
url('<?php echo $service['image'];?>'); background-size: cover; background-repeat: no-repeat;">
			<div class="ser-content">
				<div class="serv-name"><?php echo $service['title'];?> </div>
				<div class="serv-desc"><?php echo $service['description'];?> </div>
				<div class="button"><button class="blue-btn"> Speak to us now</button> 
					<?php if($service['calculator_link'] != "") {
						?>
						<a  href="<?php echo $service['calculator_link'];?>" target="_blank">&nbsp;OR &nbsp;<img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/svg/cal-btn.svg"> </a>
						<?php

					}?>
					<!-- <a  href="<?php echo $service['calculator_link'];?>" target="_blank">&nbsp;OR &nbsp;<img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/svg/cal-btn.svg"> </a> -->
				</div>
			</div>
		</div>

<?php	}
?>

</div>
</div>