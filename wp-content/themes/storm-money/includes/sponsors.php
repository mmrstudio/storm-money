<div class="container sponsors">
	<h1> Connect with trusted lenders </h1>
	<p>Our brokers have access to a large suite of tailored home loan solutions from lenders across Australia.</p>

	<div class="spon_logos">
	<?php $slogos = get_field('banks','option');
	foreach($slogos as $logos) { ?>
		<div class="item col-md-2 col-xs-6 col-sm-4 even">

			<img src="<?php echo $logos['image'];?>">
		</div>

	<?php }	?>
</div>
</div>