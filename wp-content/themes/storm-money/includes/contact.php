 <div class="contact container">
 
 

 <div class=" contact-form col-md-12">
 	<div class="form col-md-7 col-lg-8">
	 	<div class="form-title">Contact us today </div>

	 		<script type="text/javascript" src="https://mlb-newage.formstack.com/forms/js.php/sm_lead_form"></script><noscript><a href="https://mlb-newage.formstack.com/forms/sm_lead_form" title="Online Form">Online Form - Storm_Money_Lead_Form</a></noscript>
		</div>

		<div class="text col-md-5 col-lg-4">
			<div class="contact-title">Storm Money Head Office </div>
			<div class="contact-address"><?php echo get_field('contact_address','option');?>

				<div class="email"><a href="mailto:enquiries@stormmoney.com.au"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/core/images/svg/email.svg"> &nbsp; enquiries@stormmoney.com.au </a> </div>
 				<div class="phone"><a href="tel:1800 319 565"><img src="<?php echo get_stylesheet_directory_uri(); ?>/core/images/svg/phone.svg"> &nbsp; 1800 319 565 </a></div>

			 </div>
			 <div class="contact-map">
				 	<iframe class="resp-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.498884750816!2d144.98154155169158!3d-37.8252050796507!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad642bd87ff1437%3A0xc4a1ea06671d4d1d!2s60+Olympic+Blvd%2C+Melbourne+VIC+3004!5e0!3m2!1sen!2sau!4v1563254614639!5m2!1sen!2sau" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
				 </div> 

	</div>

	</div>

	
  
</div>
 

 <div class="thanks-title">Thank you </div>
<div class="thanks-message">
	We appreciate you contacting us about [Contact Reason]. One of our brokers will get back to you shortly. <br/>
	Alternatively, please call us direct on 1800 319 565 to talk to one of our staff members.
</div>