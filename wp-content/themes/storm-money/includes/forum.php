<div class="container forum">

	<div class="forum-title center"> <?php echo get_field('forum_title','option'); ?></div>
	<div class="forum-desc center"><?php echo get_field('forum_description','option');?> </div>

	<div class="fan-list">
		<?php $fans = get_field('fans','option');
		foreach ($fans as $fan) { ?>

			<div class="col-md-4 col-sm-4">
				<div class="fan-image"><img src="<?php echo $fan['image']; ?>"> </div>
				<div class="fan-title"><?php echo $fan['name']; ?> </div>
				<div class="fan-position"><?php echo $fan['position']; ?> </div>
				<div class="fan-desc"><?php echo $fan['description']; ?> </div>
			</div>
		<?php }
		?>
	</div>
</div>