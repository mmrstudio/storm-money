<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); 

?>
<div id="content"  style="background:#eee url('<?php echo get_field('page_background','option'); ?>');  background-position: center;  background-repeat: no-repeat;    background-size: cover;"> 
<div class="container">
	<div id="main-content" class="row news-archive">
		<div class="container">
			<div class="col-md-12 post-content">
			<?php  //get_template_part( 'loop-header' ); ?>
			<?php if( is_category() || is_tag() || is_author() || is_date() ) { ?>
			<div class="headering-top-archive">
			<div class="col-md-5">	<h1 class="news-title">
					<?php
					if( is_day() ) :
						printf( __( 'Daily Archives: %s', 'responsive' ), '<span>' . get_the_date() . '</span>' );
					elseif( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'responsive' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
					elseif( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'responsive' ), '<span>' . get_the_date( 'Y' ) . '</span>' );
					elseif( is_category()) :
						printf(__('%s'), single_cat_title('', false));
					else :
						_e( 'News Archives', 'responsive' );
					endif;
					?>
				</h1> </div>
				<div class="cat-drop-archive col-md-7">
				<form id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">

		<?php
		$args = array(
			'show_option_none' => __( 'Select category' ),
			'show_count'       => 1,
			'orderby'          => 'name',
			'echo'             => 0,
		);
		?>

		<?php $select  = wp_dropdown_categories( $args ); ?>
		<?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
		<?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

		<?php echo $select; ?>

		<noscript>
			<input type="submit" value="View" />
		</noscript>

	</form>
</div>
			</div>
				<?php } ?>
				<?php if (isset($landing_page_object->ID)){ ?>
				<?php if( !empty($header_image) ){ ?>
						<h1 class="page-title-landing"><?php printf(__('%s'), single_cat_title('', false));?></h1>
				<?php } } ?>
	<?php if( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

		<div class="col-sm-6 col-md-3 news-box">
			<?php if ( has_post_thumbnail()) :
        $thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
        $slider_img = $thumb_image_url[0];
      endif; ?>
		 <div class="feature-img" style="background: url('');background-size: cover;background-repeat: no-repeat;background-position: center;"><img src="<?php echo $slider_img;?>" alt="" /> </div>
		<a href="<?php the_permalink(); ?>">
			
			<h4><?php echo ShortenText( 40, get_the_title(), false ); 
			//echo softTrim(the_title(), 6);?></h4>
			<div class="rel-article-time">Posted on <?php echo the_time('jS F, Y') ?></div>
		</a>
			

			
		
			<?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
		</div>
		<?php //get_template_part( 'post-data' ); ?>
		<?php endwhile;

		get_template_part( 'loop-nav' );

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	?>
				</div>
			<!-- end col-1 -->
			<?php //get_sidebar(); ?>
		</div><!-- end row -->
		</div>
	</div><!-- end of #content -->
	</div>
<?php get_footer(); ?>
