	<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
	<!doctype html>
	
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<?php wp_head(); ?>
		<?php wp_head(); ?>
		 
		
	
	</head>

<body <?php body_class(); ?>>

 <div class="header">
 	<div class="top-logo"> <a href="<?php echo home_url( ); ?>" > <img src="<?php echo get_field('top_logo','option');?>"></a></div>
 	<div id="nav-container" >
	<div class="menu">
	<?php wp_nav_menu( array(
					'container'       => 'div',
					'container_class' => 'main-nav',
					'theme_location'  => 'top-menu',
					'menu'  => 'Top Menu'
				)
			);

			?>
	</div>
</div>
<div class="button"> <a href="#" class="apply-now"><img src="<?php bloginfo('stylesheet_directory'); ?>/core/images/button.png"> </div></a>
 </div>
 

 
<!-- Nav Wrap END -->

