
<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
/*
 Template Name: Contact
 */
get_header(); 

$queried_object = get_queried_object();

$landing_page_object = get_field('landing_page_relationship', $queried_object);
// Custom header as background image
$header_image = get_field('custom_header_image', $landing_page_object->ID);
if (isset($landing_page_object->ID)){
	echo '<style type="text/css">'.get_post_meta($landing_page_object->ID, '_custom_css', true).'</style>';
	$landing_class = " landing-hero";
}
if( !empty($header_image) ){ ?>
<style>
.custom-header-img {
	background-image: url('<?php echo $header_image['sizes'][ 'custom-header' ]; ?>');
}
</style>
<?php } ?>
<?php  
	include(locate_template('includes/banners.php')); 
	
 
$menu_id = get_field('sub_menu', $landing_page_object);
if($menu_id){
?>
<div class="landing-nav">
	<div class="container">
		<?php wp_nav_menu( array(
							   'container'       => 'div',
							   'container_class' => 'landing-menu',
							   'fallback_cb'     => 'responsive_fallback_menu',
							   'menu'  => $menu_id
						   )
		);
		?>		
	</div>
</div><!-- /.landing-nav -->
<?php } ?>
<div id="content"  style="background: #eee url('<?php echo get_field('page_background','option'); ?>');  background-position: center;  background-repeat: no-repeat;    background-size: cover;">

	<div id="main-content" class="contact">
		
		<div class="container">
		<div class="headering-top">
			<div class="col-md-9">
			<h1 style="padding-bottom:0;"><?php the_title(); ?></h1>
			</div>
			<div class="col-md-3">
				<?php  get_template_part( 'loop-header' ); ?>
			</div>
		</div>
			
		<div id="content" class="col-1" style="clear:both;">
			<div class="col-sm-3"> 

					<?php $id2 = get_the_ID();
					echo do_shortcode('[wpb_childpages]'); // Get list of parent pages
				
				?>

			
		
			</div>

			<div class="col-sm-9 col-xs-12"> 
				<div class=""><div class="google-maps"><?php echo get_field('map');?> </div></div>
				<div class="col-sm-4 col-xs-6 contact-details"><?php echo get_field('contact_details');?> </div>
				<div class="col-sm-8 col-xs-6 contact-form"><?php echo get_field('contact_form_shortcode');?> </div>
			</div>
		
		</div><!-- end col-1 -->
			<?php // get_sidebar('archives'); ?>
		</div><!-- end row -->
	
</div><!-- end of #content -->

<?php get_footer(); ?>