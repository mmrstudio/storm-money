<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Template Name: Club Finder
 */

get_header();

if(isset($_GET['search-value'])){
	$search_value = $_GET['search-value'];	
}else{
	$search_value = 2000;
}


?>
<div id="content"  style="background: #eee url('<?php echo get_field('page_background','option'); ?>');  background-position: center;  background-repeat: no-repeat;    background-size: cover;">
	<div class="container">
		<div id="content-wrap" class="row">
		<div class="headering-top">
			<div class="col-md-9">
			<h1 style="padding-bottom:0;"><?php the_title(); ?></h1>
			</div>
			<div class="col-md-3">
				<?php  get_template_part( 'loop-header' ); ?>
			</div>
		</div>
			
				<div class="col-md-12">
				
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				

					<?php the_content(); ?>
					<iframe style="border-top: 20px solid #DEDEDE" id="mfcplay" name="mfcplay" src="https://membership.sportstg.com/v6/MapFinder/mapfinder.cgi?if=1&r=2&sr=1&club_level_only=1&type=1&centre_search_type=2&search_value=<?php echo $search_value; ?>" frameborder="0" allowtransparency="true"></iframe>
				<?php endwhile; else : ?>
					<h1>Post Not Found</h1>
				<?php endif; ?>
				</div>
			
				<?php //get_sidebar(); ?>
		</div><!-- end row -->
	</div><!-- end of .container -->
	
</div><!-- end of .container -->

<?php get_footer(); ?>