
<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
/*
 Template Name: Thank you
 */
get_header(); 

?>
<div id="content" >
	<div class="container">
	<div class="thanks-title">Thank you </div>
		<div class="thanks-message">
		             <p>   We appreciate you contacting us. One of our brokers will get back to you shortly. <br/>
		                Alternatively, please call us direct on 1800 319 565 to talk to one of our staff members.
		            </p>
		</div>
	</div>
</div><!-- end of #content -->

<?php get_footer(); ?>