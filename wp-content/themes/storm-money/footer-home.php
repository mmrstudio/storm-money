<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
  
</div>
<div class="footer">
 
		<div class="text"> 
		 <?php echo get_field('text','option'); ?>
		</div>

		<div class="logos">	

			<?php $logos = get_field('footer_logos','option');
			foreach($logos as $logo) { ?>
				<div> <a href="<?php echo $logo['link'];  ?>"><img src="<?php echo $logo['image'];  ?>"></a>  </div>
			<?php }
			?>
		</div>
 
</div>
 

<?php wp_footer(); ?>
<script>
	//new UISearch( document.getElementById( 'sb-search' ) );
</script>
</body>
</html>