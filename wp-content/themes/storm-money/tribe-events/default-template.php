<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>
<div id="content-wrap" class="row" style="background:#eee url('<?php echo get_field('page_background','option'); ?>');  background-position: center;  background-repeat: no-repeat;    background-size: cover;">
	<div class="container">
	
			<div class="headering-top">
				<div class="col-md-9">
					<h1 style="padding-bottom:0;" class="eventpage">
					<?php echo get_field('calendar_page_heading','option');?></h1>
				<h1 style="padding-bottom:0;" class="single-event">
					<?php echo tribe_get_events_title() ?></h1>
				</div>
				<div class="col-md-3">
					<?php  get_template_part( 'loop-header' ); ?>
				</div>
		</div>
			<div id="content" class="col-1" style="clear:both;">
					<div  class="tribe-events-pg-template">
	<?php tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
</div> <!-- #tribe-events-pg-template -->
	</div>					
		</div><!-- end row -->
	</div><!-- end of .container -->
	<?php //get_template_part( 'includes/sponsors' ); ?>
</div><!-- end of .container -->



<?php
get_footer();
