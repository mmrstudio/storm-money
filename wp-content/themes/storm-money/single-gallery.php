<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); ?>
  
  <div id="content-wrap" class="row" style="background:#eee url('<?php echo get_field('page_background','option'); ?>');  background-position: center;  background-repeat: no-repeat;    background-size: cover;">
	<div class="container">
    <div id="content-wrap">
  <div class="col-md-12">
      <div class="headering-top">
      <div class="col-md-6">
      <h1 style="padding-bottom:0;">Photo Gallery</h1>
      </div>
      <div class="col-md-6">
        <?php  get_template_part( 'loop-header' ); ?>
      </div>
    </div>
    </div>
			<div class="col-md-12 post-content">
     <div class="gallery-title-inner"><?php the_title(); ?> </div>
    <div class="photo-count"> <?php echo count(get_field('gallery_images'));?> PHOTOS</div>

           <?php 

$images = get_field('gallery_images');
//print_r($images);
if( $images ): ?>
    <ul style="text-align: center;
    padding-left: 0px;">
    
        <?php foreach( $images as $image ): ?>
                <div class="col-sm-6 col-md-3 single-gallery-div">
                <a href="<?php echo $image['url']; ?>" rel="lightbox">
                     <img src="<?php echo $image['sizes']['large']; ?>" class="gallery"  alt="<?php echo $image['alt']; ?>" style="max-height: 200px;padding: 20px 0;" />
                    
                </a>
             
                 <div class="status">
                      Uploaded on <?php the_time('F, Y') ?>
               </div> 
               </div>
               <?php $counter++; ?>
        <?php endforeach;
       
    ?> </ul>
<?php endif; ?>
	</div><!-- end row -->
</div>
</div><!-- end of .container -->
</div>
</div>
<?php get_footer(); ?>
