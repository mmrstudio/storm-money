<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'stormmoney' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' ); //24dliz82001

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('ALLOW_UNFILTERED_UPLOADS', true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'V%^~?sr5pf/(qr1WWowb63GE<J)B_)L+92 +B`|cdkZSh6~rU9-/<|/O=m{8)/-r' );
define( 'SECURE_AUTH_KEY',  'V|ao07Jn^5X==.[I=PnMa%Uo/5 pCvE/#Td4Z&o*+H P(J#^CBDn.Ro&1{464(9,' );
define( 'LOGGED_IN_KEY',    '?&KqjT{ $YSGFM)6-duJw}3X At.@$ Y+n=4?,@otd$@ D:3^kk},yZ8{R?7ISlv' );
define( 'NONCE_KEY',        'LGvBLTG?f81oV:E(9 QBe,lV1I}yHQgwPS6AY:kV$4VNSdc6!h[2kW{)[=(>Fjc9' );
define( 'AUTH_SALT',        'DN+c9nTIN*BM_l/{})2dBP&,eVEuux82pr`;L+u_Tq8NK]KPqw**4evGvYb_NMjc' );
define( 'SECURE_AUTH_SALT', '#7S}z<4O5(uq!fpD`?MXci&[sZ=((<*!4D4f}`Hk_;&B(/CL_d-Y:SZ!O>m9S{M1' );
define( 'LOGGED_IN_SALT',   'Q$m@X``LF.b:jNy2(+!r]iS&aWCmQr<]|)=$|3Y<Hv.Z_=j`xCHiM/gF_RffN^1i' );
define( 'NONCE_SALT',       'u)@]R3:Fcu^dS9[K%YeyzrR:IWf8QjHDI3otX~G7JhC!J.e=1sEn.1ZQE)kEVI)L' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
